<?php

class LimeBlastBransomImport_Module extends Core_ModuleBase
{

	/**
	 * API code used by the import cart
	 * @var string
	 */
	protected $categoryApiCode = 'bransom-import';

	/**
	 * Path to file, relative to $_SERVER['DOCUMENT_ROOT']
	 * @var string
	 */
	protected $pathToFile = '/import/bransom.csv';

	/**
	 * Creates the module information object
	 * @return Core_ModuleInfo
	 */
	protected function createModuleInfo()
	{
		return new Core_ModuleInfo(
			"Bransom CSV Import",
			"CSV import module for passing data from a Bransom POS system into Lemonstand",
			"The Lime Blast Collective");
	}

	/**
	 * Registers URL which, once activated via cron, does the import
	 * http://lemonstand.com/docs/using_module_access_points/
	 * @return array
	 */
	public function register_access_points()
	{
		return array(
			'import_bransom' => 'do_import'
		);
	}

	/**
	 * Responsible for triggering the import
	 */
	public function do_import()
	{
		$obj = new self();
		$obj->import();
	}

	public function import()
	{
		/* create import object */
		$import_model = new Shop_ProductCsvImportModel();

		/* we're not getting categories from the feed, so find the correct category to put the items in */
		$import_model->auto_create_categories = false;
		$import_model->categories = array(
			Shop_Category::create()->find_by_code($this->categoryApiCode)->id
		);

		/* we're not importing images */
		$import_model->import_product_images = false;
		// If product images are stored in a directory on the server,
		// use the next line
		//$import_model->images_directory_path = $_SERVER['DOCUMENT_ROOT'] . '/lemonstand/cron/data/images';

		// If images are stored in a ZIP archive, use the following code
		//$file = Db_File::create()->fromFile('/users/elf/temporary/images.zip');
		//$file->master_object_class = get_class($import_model);
		//$file->field = 'images_file';
		//$import_model->images_file->add($file, 'tmp_session');

		/* we are importing manufacturers */
		$import_model->auto_manufacturers = true;

		/* we are updating existing products */
		$import_model->update_existing_sku = true;

		/* we are not importing product groups */
		$import_model->auto_create_product_groups = false;

		/* we're setting the tax class for all products to 'Product' */
		$import_model->auto_tax_classes = false;
		$import_model->tax_class = Shop_TaxClass::create()->find_by_name('Product');

		/* we're setting the type of all products to 'Goods' */
		$import_model->product_type = Shop_ProductType::create()->find_by_name('Goods');

		/* Import products */
		$data_model = new Shop_Product();
		$delimiter = ',';
		$first_row_titles = true; // The first row in the CSV contains column titles

		$columns = array(
			array('name'),
			array('short_description'),
			array('manufacturer_link'),
			array('price'),
			array('sku'),
			array('in_stock'),
			array('options'),
			array('csv_import_om_flag'),
			array('csv_import_om_parent_sku'),
		); // see full list of supported product columns at the bottom of this file

		$import_result = $import_model->import_csv_data(
			$data_model,
			'tmp_session',
			$columns,
			$this,
			$delimiter,
			$first_row_titles
		);

		echo "<pre>";
		var_dump($import_result);
		echo "</pre>";

		return $import_result;
	}

	public function csvImportDbColumnPresented(&$matches, $column_db_name)
	{
		return true;
	}

	public function csvImportGetCsvFileHandle()
	{
		$handle = @fopen($_SERVER['DOCUMENT_ROOT'] . $this->pathToFile, "r");
		if (!$handle) {
			throw new Phpr_ApplicationException('Unable to open the CSV file');
		}

		return $handle;
	}

	public function csvImportBoolValue($value)
	{
		$value = mb_strtolower($value);

		if ($value == 1 || $value == 'enabled' || $value == 'y' || $value == 'yes' || $value == 'active') {
			return true;
		}

		return false;
	}

	public function csvImportFloatValue($value)
	{
		$value = str_replace(' ', '', $value);
		$value = str_replace(',', '', $value);

		return $value;
	}

	public function csvImportNumericValue($value)
	{
		$value = str_replace(' ', '', $value);
		$value = round(str_replace(',', '', $value));

		return $value;
	}
}

// Below is a full list of supported product columns
// (I think it's incomplete/inaccurate)
/*
	name
	url_name
	description
	short_description
	manufacturer_link
	images
	price
	enabled
	disable_completely
	tax_class
	sku
	weight
	width
	height
	depth
	enable_perproduct_shipping_cost
	perproduct_shipping_cost
	track_inventory
	in_stock
	hide_if_out_of_stock
	stock_alert_threshold
	expected_availability_date
	allow_pre_order
	meta_description
	meta_keywords
	categories
	grouped_attribute_name
	grouped_option_desc
	options
	product_extra_options
	extra_option_sets
	xml_data
	csv_import_parent_sku
	csv_related_sku
	grouped_sort_order
	csv_import_om_flag
*/